﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegOwin.Entities
{
    public class NotebookParameterNames
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}