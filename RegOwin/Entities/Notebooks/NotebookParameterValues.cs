﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegOwin.Entities
{
    public class NotebookParameterValues
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public NotebookParameterNames ParametrName { get; set; }
        public int ParametrNameId { get; set; }
        public List<Notebook> Notebooks { get; set; }
    }
}