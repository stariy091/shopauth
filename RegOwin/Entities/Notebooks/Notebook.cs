﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegOwin.Entities
{
    public class Notebook
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public List<NotebookParameterValues> Parametres { get; set; }
    }
}