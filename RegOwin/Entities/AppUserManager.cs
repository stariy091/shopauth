﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using RegOwin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegOwin.Entities
{
    public class AppUserManager : UserManager<AppUser>
    {
        public AppUserManager(IUserStore<AppUser> store) : base(store){}
        public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> options,IOwinContext context)
        {
            var provider = new Microsoft.Owin.Security.DataProtection.DpapiDataProtectionProvider("ASP.NET IDENTITY");
            AppContext db = context.Get<AppContext>();
            AppUserManager manager = new AppUserManager(new UserStore<AppUser>(db));
            manager.UserTokenProvider = new DataProtectorTokenProvider<AppUser>(provider.Create("EmailConfirmation"));
            manager.EmailService = new EmailService();
            return manager;
        }
    }
}