﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RegOwin.Entities
{
    public class AppContext:IdentityDbContext<AppUser>
    {
        public AppContext():base("EntityDB"){ }
        public static AppContext Create()
        {
            return new AppContext();
        }
        public DbSet<IdentityUserLogin> Logins { get; set; }
        public DbSet<Notebook> Notebooks { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartLine> CartLine { get; set; }
        public DbSet<NotebookParameterNames> ParameterNames { get; set; }
        public DbSet<NotebookParameterValues> ParameterValues { get; set; }

    }
}