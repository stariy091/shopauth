﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegOwin.Entities
{
    public class Cart
    {
        public int Id { get; set; }
        public List<CartLine> CartLines { get; set; }
        public string UserId { get; set; }
        public AppUser User { get; set; }
        public Cart()
        {
            CartLines = new List<CartLine>();
        }
    }
    public class CartLine
    {
        public int Id { get; set; }
        public int NotebookId { get; set; }
        public Notebook Notebook { get; set; }
        public int Count { get; set; }
        public Cart Cart { get; set; }
        public int CartId { get; set; }
    }
}