﻿using RegOwin.Entities;
using System;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Owin;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.Owin;
using RegOwin.Models;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using Microsoft.Owin.Security;
using RegOwin.Models.Account;
using System.Text;
using System.Threading;
using System.Net;
using RegOwin.Constants;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace RegOwin.Controllers
{
    public class AccountController : Controller
    {
        private IAuthenticationManager AuthManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }
        [HttpPost]
        public string Registration(RegistrationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (UserManager.FindByEmail(model.Email) != null)
                {
                    ModelState.AddModelError("", "пользователь с почтовым адрессом " + model.Email + " уже существует");
                    return ErrorsHtml(model);
                }
                AppUser user = new AppUser
                {
                    Email = model.Email,
                    UserName = model.Name,

                };
                IdentityResult identity = UserManager.Create(user, model.Password);
                if (identity.Succeeded)
                {
                    sendEmailToken(user.Id);
                    return "OK";
                }
                else
                {
                    foreach (var error in identity.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

            }
            return ErrorsHtml(model);
        }
        [HttpPost]
        public string Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                AppUser user = UserManager.Find(model.Name, model.Password);
                if (user != null)
                {
                    if (user.EmailConfirmed)
                    {
                        ClaimsIdentity claim = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                        AuthManager.SignOut();
                        AuthManager.SignIn(new AuthenticationProperties
                        {
                            IsPersistent = true
                        }, claim);
                        return "OK";
                    }
                    else
                    {
                        string url = "sendEmailToken?userId=" + user.Id + "";//Url.RouteUrl("fhfgh", new { controller = "Account", action = "sendEmailToken", userId = user.Id });
                        ModelState.AddModelError("", "Ваша почта не подтверждена, " + "<a href=" + url + " id=\"sendEmail\">отправить письмо повторно </a><script>sendEmail()</script>");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный логин или пароль");
                }
            }
            return ErrorsHtml(model);
        }
        public ActionResult Logout()
        {
            AuthManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Index(string returnUrl, IndexViewModel model)
        {
            if (model == null)
            {
                model = new IndexViewModel
                {
                    ReturnUrl = returnUrl
                };
            }
            return View(model);
        }
        private string ErrorsHtml(object model)
        {
            StringBuilder errors = new StringBuilder();
            errors.AppendLine("<ul>");

            foreach (var values in ModelState.Values)
            {
                foreach (var error in values.Errors)
                {
                    errors.AppendLine("<li>" + error.ErrorMessage + "</li>");
                }

            }
            errors.AppendLine("</ul>");

            return errors.ToString();
        }
        public ActionResult ConfirmUrl(string code, string userId)
        {
            if (!UserManager.FindById(userId).EmailConfirmed)
            {
                var result = UserManager.ConfirmEmail(userId, HttpUtility.UrlDecode(code));
                if (result.Succeeded)
                {
                    UserManager.AddToRole(userId, "user");
                    return View();
                }
            }
            return RedirectToAction("Index");
        }
        public void sendEmailToken(string userId)
        {
            var code = UserManager.GenerateEmailConfirmationToken(userId);
            string callbackUrl = Url.RouteUrl(null, new { controller = "Account", action = "ConfirmUrl", code = HttpUtility.UrlEncode(code), userId = userId }, protocol: Request.Url.Scheme);
            UserManager.SendEmail(userId, "Подтверждение электронной почты",
               "Для завершения регистрации перейдите по ссылке:: <a href=\""
                                               + callbackUrl + "\">завершить регистрацию</a>");
        }
        [HttpGet]
        public ViewResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public ViewResult ForgotPassword(string email)
        {
            AppUser user = UserManager.FindByEmail(email);
            if (user != null)
            {
                var code = UserManager.GeneratePasswordResetToken(user.Id);
                string callbackUrl = Url.RouteUrl(null, new { controller = "Account", action = "ResetPassword", code = HttpUtility.UrlEncode(code), userId = user.Id }, protocol: Request.Url.Scheme);
                UserManager.SendEmail(user.Id, "Сброс пароля",
                   "Для сброса пароля перейдите по ссылке:: <a href=\""
                                                   + callbackUrl + "\">сбросить пароль</a>");
                ModelState.AddModelError("", "На указанный emal отправленно сообщение с  дальнейшей инструкцией");
            }
            else
                ModelState.AddModelError("", "Пользователя с почтой " + email + " не существует");
            return View();
        }
        public ViewResult ResetPassword(string code, string userId)
        {
            var model = new ResetPasswordViewModel
            {
                Code = code,
                UserId = userId
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = UserManager.ResetPassword(model.UserId, HttpUtility.UrlDecode(model.Code), model.Password);
                if (result.Succeeded)
                {
                    TempData["message"] = "ваш пароль успешно изменен";
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult LoginVkontakte()
        {
        https://oauth.vk.com/authorize?client_id=1&display=page&redirect_uri=http://example.com/callback&scope=friends&response_type=code&v=5.53
            string url = "https://oauth.vk.com/authorize?"+
                            "client_id=" + VkApi.ClientId +
                            "&display=page"+
                            "&redirect_uri=" + "http://localhost:55782/Account/LoginVk" +
                            "&scope=friends&response_type=code&v=5.53";
            return Redirect(url);
        }
        [HttpGet]
        public ActionResult LoginVk(string code)
        {
            string userId = null;
            string token = null;
            string avatar = null;
            string url = "https://oauth.vk.com/access_token?" +
                           "client_id=" + VkApi.ClientId +
                           "&client_secret=" + VkApi.ClientSecret +
                           "&redirect_uri=" + VkApi.RedirectUriLogin +
                           "&code=" + code;
            using (WebClient client = new WebClient())
            {

                var response = client.DownloadString(url);
                var result = JsonConvert.DeserializeObject<dynamic>(response);
                userId = result["user_id"];
                token = result["access_token"];
                url = "https://api.vk.com/method/" +
                                "users.get?" +
                                "user_ids=" + userId +
                                "&fields=photo_50" +
                                "&access_token = " + token;
                response = client.DownloadString(url);
                JsonVk resultJson = JsonConvert.DeserializeObject<JsonVk>(response);
                avatar = resultJson.Responses[0].Avatar;
            }
            var db = HttpContext.GetOwinContext().Get<AppContext>();
            var a = db.Logins.FirstOrDefault(p => p.ProviderKey == userId);
            if (a != null)
            {
                AppUser user = UserManager.FindById(a.UserId);
                ClaimsIdentity claim = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                AuthManager.SignIn(new AuthenticationProperties
                {
                    IsPersistent = true
                }, claim);
                return RedirectToAction("Index", "Home");
            }            
            return View(new loginVkViewModel { IdVk = userId, AvatarUrl = avatar });
        }
        [HttpPost]
        public ActionResult LoginVk(loginVkViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (UserManager.FindByEmail(model.Email) != null)
                {
                    ModelState.AddModelError("", "пользователь с почтовым адрессом " + model.Email + " уже существует");
                    return View(model);
                }
                AppUser user = new AppUser { Email = model.Email, UserName = model.Login, AvatarUrl = model.AvatarUrl };
                var result = UserManager.Create(user);
                if (result.Succeeded)
                {
                    result = UserManager.AddLogin(user.Id, new UserLoginInfo("vk", model.IdVk));
                    if (result.Succeeded)
                    {
                        ClaimsIdentity claim = UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                        AuthManager.SignIn(new AuthenticationProperties
                        {
                            IsPersistent = true
                        }, claim);
                        return RedirectToAction("Index", "Home");
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }
            return View(model);
        }

        [DataContract]
        class JsonVk
        {
            [DataMember(Name = "response")]
            public ResponseVk[] Responses { get; set; }
        }
        [DataContract]
        class ResponseVk
        {
            [DataMember(Name = "uid")]
            public string Id { get; set; }
            [DataMember(Name = "first_name")]
            public string FirstName { get; set; }
            [DataMember(Name = "last_name")]
            public string LastName { get; set; }
            [DataMember(Name = "photo_50")]
            public string Avatar { get; set; }
        }
    }
}