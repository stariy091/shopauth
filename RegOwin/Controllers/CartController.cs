﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using RegOwin.Entities;
using RegOwin.Models.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using RegOwin.Filtres;

namespace RegOwin.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        private AppUser CurrentUser { get { return UserManager.FindByName(HttpContext.User.Identity.Name); } }
        private AppContext Db
        {
            get { return HttpContext.GetOwinContext().Get<AppContext>(); }
        }
        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }
        // GET: Cart
        public ActionResult Index()
        {
            Cart cart = Db.Carts
                .Include(c=>c.CartLines.Select(p=>p.Notebook))                
                .FirstOrDefault(p => p.UserId == CurrentUser.Id);
            return View(cart);
        }
        public void AddToCart(int id, int count=1)
        {
            Cart cart = Db.Carts
                .Include(p=>p.CartLines)              
                .FirstOrDefault(p => p.UserId == CurrentUser.Id);
            if (cart == null)
            {
                cart = new Cart { UserId = CurrentUser.Id};
                Db.Carts.Add(cart);                
            }
            CartLine cartline = cart.CartLines.FirstOrDefault(p => p.NotebookId == id && p.CartId==cart.Id);
            if (cartline==null)
            {
                cart.CartLines.Add(new CartLine { Count = count, Cart = cart, NotebookId = id });                
            }
            else
                cartline.Count += count;

            Db.SaveChanges();
        }
    }
}