﻿using Microsoft.AspNet.Identity.Owin;
using RegOwin.Entities;
using RegOwin.Models;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System;

namespace RegOwin.Controllers
{
    public class HomeController : Controller
    {
        private int itemPerPage = 3;

        public int ItemPerPage
        {
            get { return itemPerPage; }
            set { itemPerPage = value; }
        }

        private AppContext Db
        {
            get { return HttpContext.GetOwinContext().Get<AppContext>(); }
        }
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult GetProducts(List<GetProductModel> getProducts,int page=1)

        {
            IEnumerable<Notebook> temp = new List<Notebook>();
            if (getProducts != null)
            {
                var a = getProducts
                    .GroupBy(p => p.Category, e => e.Id);
                foreach (var item in a)
                {
                    IEnumerable<Notebook> listTemp = new List<Notebook>();
                    foreach (var i in item)
                    {
                        var b = Db.ParameterValues
                            .Include(o => o.Notebooks)
                            .Where(p => p.Id.ToString() == i)
                            .SelectMany(c => c.Notebooks).ToList();
                        listTemp = listTemp.Union(b);
                    }
                    if (temp.ToList().Count != 0)
                    {
                        temp = temp.Intersect(listTemp);
                    }
                    else
                    {
                        temp = listTemp;
                    }
                }
            }
            else
            {
                temp = Db.Notebooks;
            }
            var list = temp
                     .OrderBy(p => p.Price)
                     .Skip((page - 1) * ItemPerPage)
                     .Take(ItemPerPage)
                     .ToList();
            GetProductsViewModel model = new GetProductsViewModel
            {
                Products=getProducts,
                Notebooks = list,
                PaginationInfo = new PaginationInfo
                {
                    CurrentPage = page,
                    ElementPerPage = itemPerPage,
                    TotalElement = temp.ToList().Count()
                }
            };
            return PartialView(model);
        }
        public PartialViewResult Category()
        {
            CategoryViewModel model = new CategoryViewModel();
            List<CategoryViewModel> list = new List<CategoryViewModel>();
            var a = Db.ParameterValues
                .Include(p => p.ParametrName)
                .GroupBy(p => p.ParametrName, e => e);
            foreach (var item in a)
            {
                var b = new CategoryViewModel() { Name = item.Key.Name, Values = new List<NotebookParameterValues>() };
                foreach (var q in item)
                {
                    b.Values.Add(q);
                }
                list.Add(b);
            }

            return PartialView("_Category", list);
        }
    }
}