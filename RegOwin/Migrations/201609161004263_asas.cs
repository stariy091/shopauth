namespace RegOwin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class asas : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.NotebookParameterValues", "Notebook_Id", c => c.Int());
            CreateIndex("dbo.NotebookParameterValues", "Notebook_Id");
            AddForeignKey("dbo.NotebookParameterValues", "Notebook_Id", "dbo.Notebooks", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.NotebookParameterValues", "Notebook_Id", "dbo.Notebooks");
            DropIndex("dbo.NotebookParameterValues", new[] { "Notebook_Id" });
            DropColumn("dbo.NotebookParameterValues", "Notebook_Id");
        }
    }
}
