namespace RegOwin.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class asass : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.NotebookParameterValues", "Notebook_Id", "dbo.Notebooks");
            DropIndex("dbo.NotebookParameterValues", new[] { "Notebook_Id" });
            CreateTable(
                "dbo.NotebookParameterValuesNotebooks",
                c => new
                    {
                        NotebookParameterValues_Id = c.Int(nullable: false),
                        Notebook_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.NotebookParameterValues_Id, t.Notebook_Id })
                .ForeignKey("dbo.NotebookParameterValues", t => t.NotebookParameterValues_Id, cascadeDelete: true)
                .ForeignKey("dbo.Notebooks", t => t.Notebook_Id, cascadeDelete: true)
                .Index(t => t.NotebookParameterValues_Id)
                .Index(t => t.Notebook_Id);
            
            DropColumn("dbo.NotebookParameterValues", "Notebook_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.NotebookParameterValues", "Notebook_Id", c => c.Int());
            DropForeignKey("dbo.NotebookParameterValuesNotebooks", "Notebook_Id", "dbo.Notebooks");
            DropForeignKey("dbo.NotebookParameterValuesNotebooks", "NotebookParameterValues_Id", "dbo.NotebookParameterValues");
            DropIndex("dbo.NotebookParameterValuesNotebooks", new[] { "Notebook_Id" });
            DropIndex("dbo.NotebookParameterValuesNotebooks", new[] { "NotebookParameterValues_Id" });
            DropTable("dbo.NotebookParameterValuesNotebooks");
            CreateIndex("dbo.NotebookParameterValues", "Notebook_Id");
            AddForeignKey("dbo.NotebookParameterValues", "Notebook_Id", "dbo.Notebooks", "Id");
        }
    }
}
