﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using RegOwin.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RegOwin.App_Start
{
    public static class InitializeRoles
    {
        public static void CreateRoles()
        {
            AppContext context = new AppContext();
            AppRoleManager managerRole = new AppRoleManager(new RoleStore<AppRole>(context));
            AppUserManager managerUser = new AppUserManager(new UserStore<AppUser>(context));
            if (managerUser.Users.ToList().Count == 0)
            {
                managerRole.Create(new AppRole
                {
                    Name = "admin"
                });
                managerRole.Create(new AppRole
                {
                    Name = "user"
                });
                managerUser.Create(new AppUser
                {
                    UserName = "admin",
                    Email = "admin@tut.by"
                }, "administrator");
                managerUser.AddToRole(managerUser.FindByName("admin").Id, "admin");
            }
        }
    }
}