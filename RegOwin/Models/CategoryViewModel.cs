﻿using RegOwin.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegOwin.Models
{
    public class CategoryViewModel
    {
        public string Name { get; set; }
        public List<NotebookParameterValues> Values { get; set; }
    }
}