﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegOwin.Models.Cart
{
    public class IndexViewModel
    {
        public string Name { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
    }
}