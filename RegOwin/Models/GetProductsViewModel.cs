﻿using RegOwin.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegOwin.Models
{
    public class GetProductsViewModel
    {
        public List<GetProductModel> Products { get; set; }
        public PaginationInfo PaginationInfo { get; set; }
        public IEnumerable<Notebook> Notebooks { get; set; }
    }
}