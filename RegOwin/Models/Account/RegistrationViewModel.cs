﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RegOwin.Models.Account
{
    public class RegistrationViewModel
    {
        [Required]
        [Display(Name="Логин")]
        public string Name { get; set; }
        [Required]
        [Display(Name="Электронная почта")]
        public string Email { get; set; }
        [Display(Name="Пароль")]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name="Подтверждение пароля")]
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage ="Не совпали пароли")]
        public string RePassword { get; set; }
    }
}