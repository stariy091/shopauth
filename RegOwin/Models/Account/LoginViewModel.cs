﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RegOwin.Models.Account
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name="Логин")]
        public string Name { get; set; }
        [Display(Name="Пароль")]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}