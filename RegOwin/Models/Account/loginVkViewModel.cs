﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RegOwin.Models.Account
{
    public class loginVkViewModel
    {
        [Required]
        [Display(Name = "Логин")]
        public string Login { get; set; }
        [Required]
        [Display(Name = "Электронная почта")]
        public string Email { get; set; }
        public string IdVk { get; set; }
        public string AvatarUrl { get; set; }
    }
}