﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RegOwin.Models.Account
{
    public class IndexViewModel
    {
        public string ReturnUrl { get; set; }
    }
}