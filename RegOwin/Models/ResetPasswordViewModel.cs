﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RegOwin.Models
{
    public class ResetPasswordViewModel
    {
        [Required]
        public string UserId { get; set; }
        [Required]
        public string Code { get; set; }

        [Display(Name = "Пароль")]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name = "Подтверждение пароля")]
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Не совпали пароли")]
        public string RePassword { get; set; }
    }
}