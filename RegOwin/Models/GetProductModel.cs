﻿using RegOwin.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegOwin.Models
{
    public class GetProductModel
    {
        public string Category { get; set; }
        public string Id { get; set; }
    }
}