﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegOwin.Models
{
    public class PaginationInfo
    {
        public int CurrentPage { get; set; }
        public int TotalPage
        {
            get
            {
                return TotalElement % ElementPerPage == 0 
                          ? TotalElement / ElementPerPage 
                          : TotalElement / ElementPerPage + 1;
            }
        }
        public int TotalElement { get; set; }
        public int ElementPerPage { get; set; }
    }
}